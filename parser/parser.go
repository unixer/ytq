package parser

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"log"
	"net/http"
)

func check(err interface{}, v ...any) {
	if err != nil {
		log.Fatal(err, v)
	}
}

type Video struct {
	Title, Author, Link string
}

func Search(individous, query string) []Video {
	page, err := http.Get(fmt.Sprintf("https://%s/search?q=%s", individous, query))
	check(err)
	if page.StatusCode != 200 {
		log.Fatalf("Error: %d %s", page.StatusCode, page.Status)
	}
	defer page.Body.Close()

	doc, err := goquery.NewDocumentFromReader(page.Body)
	check(err)

	var videos []Video

	doc.Find(".h-box").Each(func(i int, s *goquery.Selection) {
		var unexpected bool = false

		var title string
		var author string
		var videoLink string

		s.Find(".video-card-row").Each(func(i int, s *goquery.Selection) {
			if unexpected {
				return
			}

			if a := s.Find(".flex-left"); a.Length() == 1 {
				s = a
			}
			link, exists := s.Find("a").Attr("href")

			if !exists {
				return
			}

			const videoPrefix string = "/watch?v="
			const channelPrefix string = "/channel/"

			if link[0:len(videoPrefix)] == videoPrefix {
				title = s.Find("a").Find("p").Text()
				videoLink = "https://youtube.com" + link
			} else if link[0:len(channelPrefix)] == channelPrefix {
				author = s.Find("a").Find("p").Text()
				last := len(author) - 1
				if author[last] == '\n' {
					author = author[:last-1]
				}
			} else {
				unexpected = true
			}
		})
		if unexpected {
			return
		}

		videos = append(videos, Video{Title: title, Author: author, Link: videoLink})
	})

	return videos
}
