package main

import (
	"codeberg.org/unixer/ytq/parser"
	"fmt"
	"github.com/savioxavier/termlink"
	"log"
	"math/rand"
	"os"
)

const hyperlinks bool = true

// / You can add more individous instances here!
var individouses []string = []string{"yt.artemislena.eu"}

func getQuery() string {
	if len(os.Args) < 2 {

		log.Fatal(fmt.Sprintf("Error: You need to specify a query.\nExample: `%s cute cats`", os.Args[0]))
	}

	query := os.Args[1]
	for i := 2; i < len(os.Args); i++ {
		query += os.Args[i] + " "
	}
	return query
}

func check(err interface{}, v ...any) {
	if err != nil {
		log.Fatal(err, v)
	}
}

func main() {
	query := getQuery()

	var individous string
	if len(individous) < 2 {
		individous = individouses[0]
	} else {
		individous = individouses[rand.Intn(len(individouses)-1)]
	}

	videos := parser.Search(individous, query)

	for _, video := range videos {
		if hyperlinks {
			fmt.Println(termlink.Link(video.Title, video.Link, false), "-", video.Author)
		} else {
			fmt.Println(video.Title, "-", video.Author)
			fmt.Println(video.Link)
			fmt.Println()
		}
	}
}
