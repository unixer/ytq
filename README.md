# ytq - youtube query
Search youtube from the command line
# Usage
`ytq cute cats`
# Building
*You can find prebuilt binaries in releases*
*You can change individous instances that are used by modifying the `individouses` variable in `main.go`*
*If hyperlinks don't work in your preferred terminal, disable them by setting the `hyperlinks` variable to `false` in `main.go`*
```
  go build -ldflags=-w
```
We use `-ldflags=-w` to omit the debug information.